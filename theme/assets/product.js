$(document).ready(function(){
  if ($(window).width() > 767) {

    $(".modal-zoom").height($("body").height());
    $(".modal-zoom").width($("body").width());

    $('.image-full-icon').click(function(e){
      e.stopImmediatePropagation()
      showModal();
    });

    $('.modal-zoom .close').click(function(e){
      e.stopImmediatePropagation()
      hideModal();
    });

    $(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
        hideModal();
      }
    });

    function showModal(){
      // $('html').animate({scrollTop:0}, 'fast');//IE, FF
      // $('body').animate({scrollTop:0}, 'fast');
      $(".modal-zoom").show();
    }

    function hideModal(){
      $(".modal-zoom").hide();
    }



    //Set data to use as default for modal zoom
    var mlensConfig = {

    		lensShape: "square",                // shape of the lens (circle/square)
    		lensSize: ["200px","300px"],            // lens dimensions (in px or in % with respect to image dimensions)
    		                                    // can be different for X and Y dimension
    		borderSize: 1,                  // size of the lens border (in px)
    		borderColor: "#fff",            // color of the lens border (#hex)
    		borderRadius: 0,                // border radius (optional, only if the shape is square)
    		overlayAdapt: true,    // true if the overlay image has to adapt to the lens size (boolean)
    		zoomLevel: 2,          // zoom level multiplicator (number)
    		responsive: true       // true if mlens has to be responsive (boolean)
    	};

    $("img.zoomed-image").each(function (index, value) {
        var $this = $(this);

        //Assume that if the image is in landscape it corresponds to
        //an overview image so we use a lower zoom level
        console.log("Width "+this.naturalWidth+" Height:"+this.naturalHeight);
        if(this.naturalWidth > this.naturalHeight){
           mlensConfig.zoomLevel = 1;
        }else{
          mlensConfig.zoomLevel = 2;
        }

        $this.mlens(mlensConfig);
    });
  }

    // zoombox
  $('.thumbs a').mousedown(function(){
    $('.images a').hide();
    $('.images a[data-image-id="' + $(this).attr('data-image-id') + '"]').css('display','block');
    $('.images a').removeClass('current');
    $('.images a[data-image-id="' + $(this).attr('data-image-id') + '"]').addClass('current');

    $('.thumbs a').removeClass('active');
    $('.thumbs a[data-image-id="' + $(this).attr('data-image-id') + '"]').addClass('active');

    refreshModalCurrentPicture();
    resetMainZoom();

  });

  // zoombox
  $('.images .jcarousel-control-prev').mousedown(function(event){
      event.stopImmediatePropagation();
      showPrevImage('.images a', true, false);
      refreshModalCurrentPicture();
      resetMainZoom();
  });

   $('.images .jcarousel-control-next').mousedown(function(event){
      event.stopImmediatePropagation();
      showNextImage('.images a', true, false);
      refreshModalCurrentPicture();
      resetMainZoom();
  });

  // Modal Zoom
  $('.modal-zoom-container .jcarousel-control-prev').mousedown(function(event){
      event.stopImmediatePropagation();
      showPrevImage('.modal-zoom-container div', false, true);
  });

   $('.modal-zoom-container .jcarousel-control-next').mousedown(function(event){
      event.stopImmediatePropagation();
      showNextImage('.modal-zoom-container div', false, true);
  });

  function showNextImage(selector, selectThumbs, hasLensWrapper){

    var $mainObj = $(selector);

     $mainObj.hide();
      var $next = $(selector+".current" ).next(".main-image");
      if(!$next.length){
        $next = $(selector+'.first');
      }

      $mainObj.removeClass('current');
      $next.css('display','block');
      $next.addClass('current');

      if(hasLensWrapper){
        $next.find("div[id*='mlens_wrapper']").show();
      }


      if(selectThumbs){
        $('.thumbs a').removeClass('active');
        $('.thumbs a[data-image-id="' + $next.attr('data-image-id') + '"]').addClass('active');
      }
  }

  function showPrevImage(selector, selectThumbs, hasLensWrapper){
      var $mainObj = $(selector);

      $mainObj.hide();

      var $prev = $( selector+".current"  ).prev(".main-image");

      if(!$prev.length){
        $prev = $(selector+".last");
      }


      $prev.css('display','block');
      // $( ".images a.current" ).hide('slide', {direction: 'left'}, 1000);
      // next.show('slide', {direction: 'right'}, 1000);

      $mainObj.removeClass('current');
      $prev.addClass('current');

      if(hasLensWrapper){
        $prev.find("div[id*='mlens_wrapper']").show();
      }

      if(selectThumbs){
        $('.thumbs a').removeClass('active');
        $('.thumbs a[data-image-id="' + $prev.attr('data-image-id') + '"]').addClass('active');
      }
  }

  function refreshModalCurrentPicture(){
    var id = $(".thumbs a.active").attr('data-image-id');
    $('.modal-zoom div.zoom-image-wrapper').removeClass('active').removeClass('current');;
    $('.modal-zoom div.zoom-image-wrapper[data-image-id="' + id + '"]').addClass('active')
    .addClass("current");
  }

  $(document).keydown(function(e) {
    switch(e.which) {
        case 37: // left
        $('.jcarousel-control-prev').trigger( "mousedown" );
        break;
        case 39: // right
        $('.jcarousel-control-next').trigger( "mousedown" );
        break;
        default: return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
});

    //Main product page zoom
  $('a.main-image').panzoom({
    transition: true,
    panOnlyWhenZoomed: true,
    increment: 1,
    minScale: 1,
    maxScale: 2,
    onStart:function(){
      $(".jcarousel-control-prev").removeClass("main-image-control");
      $(".jcarousel-control-next").removeClass("main-image-control");
      $(".image-full-icon").removeClass("show-hover");
    },
    onEnd:function(){
      $(".jcarousel-control-prev").addClass("main-image-control");
      $(".jcarousel-control-next").addClass("main-image-control");
      $(".image-full-icon").addClass("show-hover");
    }
  });

  $('a.main-image').on("click", function( e ) {
    e.preventDefault();
    $(this).panzoom("zoom");

  });

  function resetMainZoom(){
    $('a.main-image').each(function( ) {
      $(this).panzoom("reset");
    });
  }
});
