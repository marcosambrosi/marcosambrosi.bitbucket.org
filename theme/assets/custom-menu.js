$(document).ready(function(){
  jQuery('.toggle').click(function(e) {
      e.preventDefault();
      jQuery('.main-menu ul').slideToggle();
  });

  jQuery('.main-menu ul li .submenu').click(function(e) {
      jQuery(this).next('.sub-menu').slideToggle();
      jQuery(this).toggleClass('submenu-hide');
  });

  jQuery(".item-search a").click(function(e){
      jQuery(".search-container").slideToggle();
      jQuery(".search-container input").val("");
      jQuery(".search-container input").focus();
  });

   if ($(window).width() > 767) {
      jQuery(".search-container").focusout(function() {
        $(this).slideToggle();
      })
   }


});
