    $(document).ready(function(){
      jQuery('.toggle').click(function(e) {
          e.preventDefault();
          jQuery('.main-menu ul').slideToggle();
      });

      jQuery('.main-menu ul li .submenu').click(function(e) {
          jQuery(this).next('.sub-menu').slideToggle();
          jQuery(this).toggleClass('submenu-hide');
      });


    });
